package controller;

import cluster.ClusterListener;
import cluster.ClusterManager;
import cluster.impl.ClusterManagerImpl;
import com.hazelcast.core.Member;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import model.Settings;
import service.GameController;
import service.GameListener;
import service.WordsProvider;
import service.impl.GameControllerImpl;
import service.impl.WordsProviderImpl;
import view.*;
import view.Character;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * @author anadilo
 * @since 23.4.2016
 */

public class MainView implements Initializable, ClusterListener, GameListener {

    private ClusterManager clusterManager = null;
    private WordsProvider wordsProvider = WordsProviderImpl.getInstance();
    private GameController gameController = GameControllerImpl.getInstance(this);

    @FXML
    private BorderPane gamePane;

    @FXML
    private FlowPane lettersPane;

    @FXML
    private FlowPane playersPane;

    @FXML
    private FlowPane solvedPane;

    @FXML
    private VBox scorePane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new Thread() {
            public void run() {
                clusterManager = new ClusterManagerImpl(MainView.this);
                clusterManager.updateSettings(new Settings("Gulliver", Character.GULLIVER));
            }
        }.start();
    }

    @Override
    public void updatePlayers(Set<Member> members) {
        Platform.runLater(() -> {
            playersPane.getChildren().clear();
            members.stream().forEach(m -> playersPane.getChildren().add(new PlayerCard(m)));
        });
    }

    @Override
    public void startGame(char[] letters) {
        wordsProvider.loadWordsWithLetters(letters);
        gameController.startGame();

        Platform.runLater(() -> {
            playersPane.setVisible(false);
            gamePane.setVisible(true);
            clusterManager.getMembers().stream().forEach(m -> scorePane.getChildren().add(new PlayerScore(m)));
        });
    }

    @Override
    public void timeTick(int secsLeft) {

    }

    @Override
    public void endGame() {

    }

    @FXML
    void ready(ActionEvent event) {
        clusterManager.ready();
    }

    private void acceptWord(String word) {
        if (wordsProvider.contains(word)) {
            gameController.calculateScore(word);
            clusterManager.updateScore(gameController.getScore());
        } else
            wordFail();
    }

    private void wordFail() {

    }

}
