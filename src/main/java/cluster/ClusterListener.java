package cluster;

import com.hazelcast.core.Member;

import java.util.Set;

/**
 * @author anadilo
 * @since 23.4.2016
 */

public interface ClusterListener {

    void updatePlayers(Set<Member> players);

    void startGame(char[] letters);

}
