package cluster.impl;

import cluster.ClusterListener;
import cluster.ClusterManager;
import com.hazelcast.config.Config;
import com.hazelcast.core.*;
import model.Score;
import model.Settings;
import org.apache.commons.lang3.BooleanUtils;
import service.WordsProvider;
import service.impl.WordsProviderImpl;

import java.util.Set;

/**
 * @author anadilo
 * @since 23.4.2016
 */

public class ClusterManagerImpl implements ClusterManager, MembershipListener, MessageListener<char[]> {

    private static HazelcastInstance hazelcastInstance;
    private ClusterListener clusterListener;
    private WordsProvider wordsProvider = WordsProviderImpl.getInstance();
    private ITopic<char[]> topicCreateGame;

    public ClusterManagerImpl(ClusterListener clusterListener) {
        hazelcastInstance = Hazelcast.newHazelcastInstance(new Config());
        hazelcastInstance.getCluster().addMembershipListener(this);
        this.clusterListener = clusterListener;

        topicCreateGame = hazelcastInstance.getTopic("create");
        topicCreateGame.addMessageListener(this);

        clusterListener.updatePlayers(getMembers());
    }

    @Override
    public void memberAdded(MembershipEvent membershipEvent) {
        clusterListener.updatePlayers(getMembers());
    }

    @Override
    public void memberRemoved(MembershipEvent membershipEvent) {
        clusterListener.updatePlayers(getMembers());
    }

    @Override
    public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
        clusterListener.updatePlayers(getMembers());

        if (memberAttributeEvent.getKey().equals("ready"))
            checkEveryoneReady();
    }

    @Override
    public Set<Member> getMembers() {
        return hazelcastInstance.getCluster().getMembers();
    }

    @Override
    public Member getMe() {
        return hazelcastInstance.getCluster().getLocalMember();
    }

    @Override
    public void updateSettings(Settings settings) {
        Member me = getMe();

        me.setStringAttribute("name", settings.getName());
        me.setStringAttribute("character", settings.getCharacter().name());
    }

    @Override
    public void updateScore(Score score) {
        Member me = getMe();

        me.setIntAttribute("words", score.getWords());
        me.setIntAttribute("points", score.getPoints());
    }

    @Override
    public boolean isLeader() {
        return getMe().getUuid().equals(getLeader().getUuid());
    }

    @Override
    public void ready() {
        getMe().setBooleanAttribute("ready", true);
    }

    @Override
    public void onMessage(Message<char[]> message) {
        clusterListener.startGame(message.getMessageObject());
    }

    private void checkEveryoneReady() {
        for (Member member : getMembers())
            if (!BooleanUtils.isTrue(member.getBooleanAttribute("ready")))
                return;

        if (isLeader())
            createGame();
    }

    private void createGame() {
        char[] letters = wordsProvider.chooseLetters();
        topicCreateGame.publish(letters);
    }

    public static void shutdown() {
        hazelcastInstance.shutdown();
    }

    private Member getLeader() {
        return getMembers().iterator().next();
    }
}
