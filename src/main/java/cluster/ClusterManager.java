package cluster;

import com.hazelcast.core.Member;
import model.Score;
import model.Settings;

import java.util.Set;

/**
 * @author anadilo
 * @since 23.4.2016
 */

public interface ClusterManager {

    Set<Member> getMembers();

    Member getMe();

    boolean isLeader();

    void ready();

    void updateSettings(Settings settings);

    void updateScore(Score score);

}
