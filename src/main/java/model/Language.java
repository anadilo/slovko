package model;

/**
 * @author anadilo
 * @since 23.4.2016
 */

public enum Language {

    CROATIAN("words/hr.txt"),
    ENGLISH("words/en.txt");

    private String file;

    Language(String file) {
        this.file = file;
    }

    public String getFile() {
        return file;
    }
}
