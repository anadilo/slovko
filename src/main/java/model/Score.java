package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author anadilo
 * @since 23.4.2016
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Score {

    private int words;
    private int points;

    public void incrementWords() {
        words ++;
    }

    public void incrementPoints(String word) {
        if (word.length() == 7)
            points += 14;
        else if (word.length() == 8)
            points += 20;
        else
            points += word.length();
    }
}
