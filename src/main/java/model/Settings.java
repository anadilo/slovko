package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import view.Character;

/**
 * @author anadilo
 * @since 23.4.2016
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Settings {

    private String name;
    private Character character;

}
