import cluster.impl.ClusterManagerImpl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * @author anadilo
 * @since 23.4.2016
 */

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Font.loadFont(Main.class.getResource("fonts/Sacramento-Regular.ttf").toExternalForm(), 40);
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/main-view.fxml"));
        primaryStage.setTitle("Slovko");
        primaryStage.setScene(new Scene(root));
        primaryStage.setWidth(1100);
        primaryStage.setHeight(750);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        ClusterManagerImpl.shutdown();
        System.exit(0);
    }
}
