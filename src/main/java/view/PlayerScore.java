package view;

import com.hazelcast.core.Member;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;

/**
 * @author anadilo
 * @since 28.4.2016
 */

public class PlayerScore extends HBox {

    public PlayerScore(Member member) {
        Character character = Character.valueOf(member.getStringAttribute("character") == null ? "ATTICUS" : member.getStringAttribute("character"));

        setImage(character);
        setLabels(member.getStringAttribute("name"), member.getStringAttribute("ip"));
    }

    private void setImage(Character character) {
        ImageView img = new ImageView(character.getFile());

        img.setFitHeight(60);
        img.setFitWidth(60);
        img.setClip(new Circle(30, 30, 30));

        getChildren().add(img);
    }

    private void setLabels(String name, String ip) {

    }
}
