package view;

/**
 * @author anadilo
 * @since 26.4.2016
 */

public enum Character {

    ATTICUS("images/atticus.png", "rgb(204, 223, 204)", "rgb(38, 38, 38)"),
    BYRON("images/byron.png", "rgb(12, 85, 93)", "rgb(234, 208, 171)"),
    GULLIVER("images/gulliver.png", "rgb(38, 38, 38)", "rgb(204, 223, 204)");

    private String file;
    private String cardColor;
    private String nameColor;

    Character(String file, String cardColor, String nameColor) {
        this.file = file;
        this.cardColor = cardColor;
        this.nameColor = nameColor;
    }

    public String getFile() {
        return file;
    }

    public String getCardColor() {
        return cardColor;
    }

    public String getNameColor() {
        return nameColor;
    }
}
