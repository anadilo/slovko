package view;

import com.hazelcast.core.Member;
import javafx.animation.*;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;
import lombok.Getter;
import lombok.Setter;

/**
 * @author anadilo
 * @since 26.4.2016
 */

public class PlayerCard extends BorderPane {

    private static final double CARD_WIDTH = 180;
    private static final double CARD_HEIGHT = 250;
    private static final String LABEL_FONT = "Segoe UI";

    private Character character;
    @Getter
    private String name;
    @Getter
    private String ip;
    @Setter
    private boolean ready;

    private Label lblName = new Label();
    private Label lblIp = new Label();

    public PlayerCard(Character character, String name, String ip, boolean ready, boolean isMe) {
        super();

        this.character = character;
        this.name = name;
        this.ip = ip;
        this.ready = ready;

        setSize();
        setImage();
        setLabels();
        setColors();

        if (isMe)
            animate();
    }

    public PlayerCard(Member member) {
        this(Character.valueOf(member.getStringAttribute("character") == null ? "ATTICUS" : member.getStringAttribute("character")),
                member.getStringAttribute("name") == null ? member.getUuid() : member.getStringAttribute("name"),
                member.getSocketAddress().getAddress().getHostAddress(),
                member.getBooleanAttribute("ready") == null ? false : member.getBooleanAttribute("ready"),
                member.localMember());
    }

    private void setSize() {
        setPrefSize(CARD_WIDTH, CARD_HEIGHT);
    }

    private void setImage() {
        ImageView img = new ImageView(character.getFile());

        img.setFitHeight(150);
        img.setFitWidth(150);
        img.setClip(new Circle(75, 75, 75));
        setCenter(img);
    }

    private void setLabels() {
        VBox vBox = new VBox();

        vBox.setPrefHeight(70);
        vBox.setAlignment(Pos.TOP_CENTER);

        lblName.setText(name + (ready ? " (ready)" : ""));
        lblName.setFont(Font.font(LABEL_FONT, FontWeight.BOLD, 18));
        lblName.setTextFill(Paint.valueOf(character.getNameColor()));

        lblIp.setText(ip);
        lblIp.setFont(Font.font(LABEL_FONT, FontWeight.BOLD, 14));
        lblIp.setTextFill(Paint.valueOf("rgb(255, 104, 96)"));

        vBox.getChildren().addAll(lblName, lblIp);
        setBottom(vBox);
    }

    private void setColors() {
        setStyle("-fx-background-color: " + character.getCardColor());
    }

    private void animate() {
        TranslateTransition upDown = new TranslateTransition(Duration.millis(600), this);

        upDown.setFromY(0);
        upDown.setToY(-15);
        upDown.setCycleCount(Timeline.INDEFINITE);
        upDown.setAutoReverse(true);

        upDown.play();
    }

}
