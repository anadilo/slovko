package service;

import model.Score;

/**
 * @author anadilo
 * @since 25.4.2016
 */

public interface GameController {

    void startGame();

    void calculateScore(String word);

    Score getScore();

    void endGame();
}
