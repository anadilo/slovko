package service.impl;

import model.Language;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import service.WordsProvider;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author anadilo
 * @since 23.4.2016
 */

public class WordsProviderImpl implements WordsProvider {

    private static final short LETTER_NUM = 8;

    private List<String> allWords;
    private Set<String> gameWords;
    private char[] letters;

    private WordsProviderImpl() {
        loadFile(Language.CROATIAN);
    }

    private static WordsProvider instance = new WordsProviderImpl();

    public static WordsProvider getInstance() {
        return instance;
    }

    @Override
    public void loadFile(Language language) {
        allWords = readWordsFile(language);
    }

    @Override
    public char[] chooseLetters() {
        List<String> longestWords = allWords.stream().filter(w -> w.length() == LETTER_NUM).collect(Collectors.toList());
        return longestWords.get(RandomUtils.nextInt(0, longestWords.size())).toCharArray();
    }

    @Override
    public void loadWordsWithLetters(char[] letters) {
        this.letters = letters;
        gameWords = allWords.stream().filter(this::isConstructable).collect(Collectors.toSet());
    }

    @Override
    public Set<String> getGameWords() {
        return gameWords;
    }

    @Override
    public boolean contains(String word) {
        return word != null && gameWords.contains(word.toLowerCase());

    }

    private boolean isConstructable(String word) {
        if (word.length() > LETTER_NUM || word.length() < 3)
            return false;

        if (!StringUtils.containsOnly(word, letters))
            return false;

        String lettersString = new String(letters);
        for (char c : word.toCharArray()) {
            if (StringUtils.countMatches(word, c) > StringUtils.countMatches(lettersString, c))
                return false;
        }

        return true;
    }

    private List<String> readWordsFile(Language language) {
        try {
            URL url = getClass().getClassLoader().getResource(language.getFile());
            return url != null ? IOUtils.readLines(url.openStream(), "UTF-8") : new ArrayList<>();
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

}
