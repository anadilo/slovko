package service.impl;

import model.Score;
import service.GameController;
import service.GameListener;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author anadilo
 * @since 25.4.2016
 */

public class GameControllerImpl extends TimerTask implements GameController {

    private static final int GAME_TIME = 5 * 60;

    private Timer timer;
    private Score score;
    private volatile int secsLeft;
    private GameListener gameListener;

    private GameControllerImpl(GameListener gameListener) {
        this.gameListener = gameListener;
    }

    private static GameController instance;

    public static GameController getInstance(GameListener gameListener) {
        if (instance == null)
            instance = new GameControllerImpl(gameListener);

        return instance;
    }

    @Override
    public void startGame() {
        secsLeft = GAME_TIME;
        score = new Score();
        timer = new Timer();
        timer.scheduleAtFixedRate(this, 0, 1000);
    }

    @Override
    public void calculateScore(String word) {
        score.incrementWords();
        score.incrementPoints(word);
    }

    @Override
    public Score getScore() {
        return score;
    }

    @Override
    public void endGame() {
        timer.cancel();
        gameListener.endGame();
    }

    @Override
    public void run() {
        secsLeft --;
        gameListener.timeTick(secsLeft);

        if (secsLeft == 0)
            endGame();
    }
}
