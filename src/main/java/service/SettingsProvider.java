package service;

import model.Settings;

/**
 * @author anadilo
 * @since 23.4.2016
 */

public interface SettingsProvider {

    Settings getSettings();

    void save(Settings settings);
}
