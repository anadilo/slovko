package service;

import model.Language;

import java.util.Set;

/**
 * @author anadilo
 * @since 23.4.2016
 */

public interface WordsProvider {

    void loadFile(Language language);

    char[] chooseLetters();

    void loadWordsWithLetters(char[] letters);

    Set<String> getGameWords();

    boolean contains(String word);

}
