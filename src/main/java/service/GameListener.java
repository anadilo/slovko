package service;

/**
 * @author anadilo
 * @since 25.4.2016
 */

public interface GameListener {

    void timeTick(int secsLeft);

    void endGame();
}
