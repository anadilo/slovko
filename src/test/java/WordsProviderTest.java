import model.Language;
import org.junit.Test;
import service.WordsProvider;
import service.impl.WordsProviderImpl;

import java.util.Arrays;

/**
 * @author anadilo
 * @since 23.4.2016
 */

public class WordsProviderTest {

    @Test
    public void test() {
        WordsProvider wordsProvider = WordsProviderImpl.getInstance();
        wordsProvider.loadFile(Language.CROATIAN);
        char[] letters = wordsProvider.chooseLetters();
        System.out.println(Arrays.toString(letters));

        wordsProvider.loadWordsWithLetters(letters);
        System.out.println(wordsProvider.getGameWords().size());
        wordsProvider.getGameWords().stream().forEach(System.out::println);
    }
}
